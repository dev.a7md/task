import {Component, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {StudentModel} from '../student.model';
import {StudentsService} from '../students.service';

@Component({
  selector: 'app-student-preview',
  templateUrl: './student-preview.component.html',
  styleUrls: ['./student-preview.component.scss']
})
export class StudentPreviewComponent {
  @Input() student: StudentModel;
  @Input() index: number;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private studentsService: StudentsService) {
  }

  viewStudentDetail() {
    this.router.navigate(['/details']);
    this.studentsService.selectedStudent.emit(this.student);
    this.studentsService.selectedIndex.emit(this.index);
  }
}
