export interface Address {
  state: string;
  country: string;
}

export class StudentModel {
  constructor(
    public id: string,
    public name: string,
    public age: number,
    public faculty: string,
    public phone: string,
    public dob: string,
    public img: string,
    public address: Address
  ) {
  }
}
