import {Injectable} from '@angular/core';

declare var toastr: any;

@Injectable({
  providedIn: 'root'
})
export class ToastrService {
  constructor() {
    this.toastrOption();
  }

  success(message: string, title?: string) {
    toastr.success(message, title);
  }

  toastrOption() {
    toastr.options = {
      'closeButton': false,
      'debug': false,
      'newestOnTop': false,
      'progressBar': true,
      'positionClass': 'toast-bottom-full-width',
      'preventDuplicates': true,
      'onclick': null,
      'showDuration': '500',
      'hideDuration': '1000',
      'timeOut': '2500',
      'extendedTimeOut': '1000',
      'showEasing': 'swing',
      'hideEasing': 'linear',
      'showMethod': 'fadeIn',
      'hideMethod': 'fadeOut'
    };
  }
}
