import {Component, OnInit} from '@angular/core';

import {StudentModel} from '../student.model';
import {StudentsService} from '../students.service';
import {ModalService} from './modal.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.scss'],
  providers: [ModalService]
})
export class StudentDetailsComponent implements OnInit {
  selectedStudent: StudentModel;
  index: number;

  constructor(private studentsService: StudentsService, private modalService: ModalService) {
  }

  ngOnInit() {
    this.studentsService.selectedStudent.subscribe(
      student => this.selectedStudent = student
    );
    this.studentsService.selectedIndex.subscribe(
      index => this.index = index
    );
  }

  deleteStudent() {
    this.modalService.modalOpen.emit(true);
    this.modalService.index.emit(this.index);
  }

  canDeactivate() {
    return true;
  }
}
