import {EventEmitter} from '@angular/core';

export class ModalService {
  modalOpen = new EventEmitter<boolean>();
  index = new EventEmitter<number>();
}
