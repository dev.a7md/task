import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {StudentsService} from '../../students.service';
import {ModalService} from '../modal.service';
import {ToastrService} from '../../toastr.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  index: number;
  modal = false;

  constructor(private studentService: StudentsService,
              private modalService: ModalService,
              private toastrService: ToastrService,
              private router: Router) {
  }

  ngOnInit() {
    this.modalService.modalOpen.subscribe(val => this.modal = val);
    this.modalService.index.subscribe(val => this.index = val);
  }

  onDeleteStudent() {
    this.studentService.deleteStudent(this.index);
    this.toastrService.success('Student Deleted');
    this.modal = false;

    this.router.navigate(['/']);
  }

  back() {
    this.modal = false;
  }
}
