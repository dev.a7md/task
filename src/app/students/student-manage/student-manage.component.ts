import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {StudentModel} from '../student.model';
import {StudentsService} from '../students.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from '../toastr.service';

@Component({
  selector: 'app-student-manage',
  templateUrl: './student-manage.component.html',
  styleUrls: ['./student-manage.component.scss']
})
export class StudentManageComponent implements OnInit {
  form: FormGroup;
  student: StudentModel;
  editMode = true;
  index: number;

  constructor(private studentsService: StudentsService,
              private route: ActivatedRoute,
              private router: Router,
              private toasteService: ToastrService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      'name': new FormControl(null,
        [
          Validators.required,
          Validators.max(50)
        ]
      ),
      'phone': new FormControl(null,
        [
          Validators.required,
          Validators.pattern('^[0-9]{3}\-[0-9]{4}-[0-9]{3}$')
        ]
      ),
      'faculty': new FormControl(null, Validators.required),
      'dob': new FormControl(null, Validators.required),
      'address': new FormGroup({
        'state': new FormControl(null),
        'country': new FormControl(null)
      }),
      'image': new FormControl()
    });

    if (this.route.url.value[2].path === 'add') {
      this.editMode = false;
    }

    if (this.editMode) {
      this.index = +this.route.snapshot.paramMap.get('index');
      this.student = this.studentsService.getStudent(this.index);
      this.setInit();
    }
  }

  setInit() {
    this.form.patchValue({
      name: this.student.name,
      phone: this.student.phone,
      faculty: this.student.faculty,
      dob: this.student.dob,
      address: {
        state: this.student.address.state,
        country: this.student.address.country
      },
      img: this.student.img
    });
  }

  onSubmit() {
    const id = (Math.floor(Math.random() * 1000000)).toString();
    const name = this.form.controls.name.value;
    const phone = this.form.controls.phone.value;
    const faculty = this.form.controls.faculty.value;
    const dob = this.form.controls.dob.value;
    const address = {
      state: this.form.controls.address.value.state || '',
      country: this.form.controls.address.value.country || ''
    };
    const image = this.form.controls.image.value || '';

    const age = new Date().getFullYear() - new Date(dob).getFullYear();

    const student = new StudentModel(
      id,
      name,
      age,
      faculty,
      phone,
      dob,
      image,
      address
    );

    if (this.editMode) {
      this.studentsService.updateStudent(this.index, student);
      this.toasteService.success('Saved');
    } else {
      this.studentsService.addStudent(student);
      this.toasteService.success('Student Added');
    }

    this.router.navigate(['/']);
  }
}
