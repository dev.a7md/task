import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {StudentsRoutingModule} from './students-routing.module';

import {StudentsComponent} from './students.component';
import {StudentDetailsComponent} from './student-details/student-details.component';
import {StudentManageComponent} from './student-manage/student-manage.component';
import {StudentPreviewComponent} from './student-preview/student-preview.component';
import {ModalComponent} from './student-details/modal/modal.component';

@NgModule({
  declarations: [
    StudentsComponent,
    StudentDetailsComponent,
    StudentManageComponent,
    StudentPreviewComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StudentsRoutingModule
  ]
})
export class StudentsModule {
}
