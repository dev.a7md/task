import {EventEmitter, Injectable} from '@angular/core';
import {StudentModel} from './student.model';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  selectedStudent = new EventEmitter<StudentModel>();
  selectedIndex = new EventEmitter<number>();
  studentsChanged = new EventEmitter<StudentModel[]>();

  private students = [
    new StudentModel(
      'st_1',
      'Hagar Ahmed',
      27,
      '#1',
      '011234567891',
      '17/3/92',
      'https://bit.ly/2Iy6Atv',
      {
        state: 'state',
        country: 'egypt'
      }
    ),
    new StudentModel(
      'st_2',
      'Mohamed Hassan',
      24,
      '#2',
      '011234567891',
      '17/3/95',
      'https://bit.ly/2Iy6Atv',
      {
        state: 'state',
        country: 'egypt'
      }
    ),
    new StudentModel(
      'st_3',
      'Hossam Saleh',
      28,
      '#3',
      '011234567891',
      '17/3/91',
      'https://bit.ly/2Iy6Atv',
      {
        state: 'state',
        country: 'egypt'
      }
    ),
  ];

  getStudents() {
    return this.students.slice();
  }

  getStudent(index) {
    return this.students[index];
  }

  addStudent(student: StudentModel) {
    this.students.push(student);
    this.studentsChanged.emit(this.students.slice());
  }

  updateStudent(index, student) {
    this.students[index] = student;
    this.studentsChanged.emit(this.students.slice());
  }

  deleteStudent(i) {
    this.students.splice(i, 1);
    this.studentsChanged.emit(this.students.slice());
  }
}
