import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DeactivateGuard} from './deactivate.guard';

import {StudentsComponent} from './students.component';
import {StudentDetailsComponent} from './student-details/student-details.component';
import {StudentManageComponent} from './student-manage/student-manage.component';

const routes: Routes = [
  {
    path: '', component: StudentsComponent, children: [
      {path: 'details', component: StudentDetailsComponent, canDeactivate: [DeactivateGuard]}
    ]
  },
  {path: 'students/manage/:index', component: StudentManageComponent},
  {path: 'students/manage/add', component: StudentManageComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StudentsRoutingModule {
}
