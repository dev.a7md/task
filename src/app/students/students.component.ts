import {Component, OnInit} from '@angular/core';

import {StudentModel} from './student.model';
import {StudentsService} from './students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  students: StudentModel[];

  constructor(private studentsService: StudentsService) {
  }

  ngOnInit() {
    this.students = this.studentsService.getStudents();

    this.studentsService.studentsChanged.subscribe(
      students => this.students = students
    );
  }

}
